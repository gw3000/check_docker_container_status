# Check Docker Containers

Script to check if containers are excited. If so, start it.

## Docker container in Script
edit the python script 

```python
containers = ['steuer-mysql', 'steuer-nginx', 'steuer-php']
```

## Cronjob every 5 Minutes

```bash
# m h  dom mon dow   command                                                    
*/5 * * * * bash /home/gw/Documents/python/docker/check_docker_containers.sh    
```                                                                                             
