import docker

containers = ['lvr-db_worker', 'lvr-proxy', 'lvr-php', 'lvr-postgres',
        'lvr-nginx', 'portainer_agent']

for container_name in containers:
    client = docker.from_env()

    try:
        container = client.containers.get(container_name)
        status = container.attrs['State']['Status']
        if status == 'exited':
            container.start()
    except:
        print(f'Container "{container_name}" does not exist or has the status exited!')
